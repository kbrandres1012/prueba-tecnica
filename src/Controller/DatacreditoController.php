<?php

namespace App\Controller;

use App\Entity\Datacredito;
use App\Entity\Service;
use App\Entity\Customer;
use App\Form\DatacreditoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/datacredito")
 */
class DatacreditoController extends AbstractController
{
    /**
     * @Route("/", name="datacredito_index", methods={"GET"})
     */
    public function index(): Response
    {
        $customer = $this->getUser();
        $datacreditos = $this->getDoctrine()
                ->getRepository(Datacredito::class)
                ->findBy(array('customer' => $customer->getId()), array()
            );

        return $this->render('datacredito/index.html.twig', [
            'datacreditos' => $datacreditos,
        ]);
    }

    /**
     * @Route("/new", name="datacredito_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $datacredito = new Datacredito();
        $form = $this->createForm(DatacreditoType::class, $datacredito);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            //customer
            $user = $this->getUser();
            $customer = $this->getDoctrine()
                ->getRepository(Customer::class)
                ->find($user);
            $datacredito->setCustomer($customer);
            //Service
            $service = $this->getDoctrine()
                ->getRepository(Service::class)
                ->find(2);
            
            $datacredito->setService($service);
            $entityManager->persist($datacredito);
            $entityManager->flush();

            return $this->redirectToRoute('datacredito_index');
        }

        return $this->render('datacredito/new.html.twig', [
            'datacredito' => $datacredito,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="datacredito_show", methods={"GET"})
     */
    public function show(Datacredito $datacredito): Response
    {
        return $this->render('datacredito/show.html.twig', [
            'datacredito' => $datacredito,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="datacredito_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Datacredito $datacredito): Response
    {
        $form = $this->createForm(DatacreditoType::class, $datacredito);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('datacredito_index', [
                'id' => $datacredito->getId(),
            ]);
        }

        return $this->render('datacredito/edit.html.twig', [
            'datacredito' => $datacredito,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="datacredito_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Datacredito $datacredito): Response
    {
        if ($this->isCsrfTokenValid('delete'.$datacredito->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($datacredito);
            $entityManager->flush();
        }

        return $this->redirectToRoute('datacredito_index');
    }
}
