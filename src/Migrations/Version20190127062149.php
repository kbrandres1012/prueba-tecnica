<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190127062149 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY fk_company_city1');
        $this->addSql('ALTER TABLE datacredito DROP FOREIGN KEY fk_datacredito_city1');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY fk_company_department1');
        $this->addSql('ALTER TABLE datacredito DROP FOREIGN KEY fk_datacredito_department1');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY fk_company_service1');
        $this->addSql('ALTER TABLE datacredito DROP FOREIGN KEY fk_datacredito_service1');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE datacredito');
        $this->addSql('DROP TABLE department');
        $this->addSql('DROP TABLE service');
        $this->addSql('ALTER TABLE user DROP customer_id, CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE latin1_swedish_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, service_id INT NOT NULL, city_id INT NOT NULL, department_id INT NOT NULL, name VARCHAR(255) NOT NULL COLLATE latin1_swedish_ci, type VARCHAR(255) NOT NULL COLLATE latin1_swedish_ci, shareholder INT NOT NULL, capital NUMERIC(6, 4) NOT NULL, INDEX fk_company_city1_idx (city_id), INDEX fk_company_customer_idx (customer_id), INDEX fk_company_department1_idx (department_id), INDEX fk_company_service1_idx (service_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE datacredito (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, service_id INT NOT NULL, city_id INT NOT NULL, department_id INT NOT NULL, date DATETIME NOT NULL, evidence BLOB NOT NULL, INDEX fk_datacredito_city1_idx (city_id), INDEX fk_datacredito_customer1_idx (customer_id), INDEX fk_datacredito_department1_idx (department_id), INDEX fk_datacredito_service1_idx (service_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE department (id INT NOT NULL, name VARCHAR(255) NOT NULL COLLATE latin1_swedish_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE service (id INT AUTO_INCREMENT NOT NULL, name INT NOT NULL, activo VARCHAR(255) NOT NULL COLLATE latin1_swedish_ci, price NUMERIC(8, 4) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT fk_company_city1 FOREIGN KEY (city_id) REFERENCES city (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT fk_company_customer FOREIGN KEY (customer_id) REFERENCES customer (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT fk_company_department1 FOREIGN KEY (department_id) REFERENCES department (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT fk_company_service1 FOREIGN KEY (service_id) REFERENCES service (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE datacredito ADD CONSTRAINT fk_datacredito_city1 FOREIGN KEY (city_id) REFERENCES city (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE datacredito ADD CONSTRAINT fk_datacredito_customer1 FOREIGN KEY (customer_id) REFERENCES customer (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE datacredito ADD CONSTRAINT fk_datacredito_department1 FOREIGN KEY (department_id) REFERENCES department (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE datacredito ADD CONSTRAINT fk_datacredito_service1 FOREIGN KEY (service_id) REFERENCES service (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE user ADD customer_id INT NOT NULL, CHANGE roles roles TEXT NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
