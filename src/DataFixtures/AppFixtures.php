<?php

namespace App\DataFixtures;

//use App\Entity\User;
use App\Entity\City;
use App\Entity\Department;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        /*$user = new User();
        $user->setId(1);
        $user->setEmail('prueba@test.com');
        $user->setPassword(
            $this->encoder->encodePassword($user, '0000')
        );
        $user->setRoles([]);
        $manager->persist($user);
        $manager->flush(); */



        $colombiaFile = file_get_contents("src/DataFixtures/json/colombia.json");
        $colombia = json_decode($colombiaFile, true);
        foreach($colombia as $department){
            $newDepartment = new Department();
            $newDepartment->setName($department['departamento']);
            $manager->persist($newDepartment); 
            $manager->flush();
            $ciudades = $department['ciudades'];

            for($i = 0; $i < count($ciudades); $i++){
                $newCity = new City();
                $newCity->setName($ciudades[$i]);
                $newCity->setDepartment($newDepartment);
                $manager->persist($newCity); 
                $manager->flush();
            }
        }

    }
}
