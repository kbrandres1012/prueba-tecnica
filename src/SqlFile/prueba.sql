-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bd_test
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Table `bd_test`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_test`.`customer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `fullname` VARCHAR(255) NOT NULL,
  `typeid` INT(11) NOT NULL,
  `dni` INT(11) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `phone` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `bd_test`.`service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_test`.`service` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `activo` VARCHAR(255) NOT NULL,
  `price` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bd_test`.`department`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_test`.`department` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bd_test`.`city`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `bd_test`.`city` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `department_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_city_department1_idx` (`department_id` ASC),
  CONSTRAINT `fk_city_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `bd_test`.`department` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_test`.`company`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_test`.`company` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `type` VARCHAR(255) NOT NULL,
  `shareholder` INT(11) NOT NULL,
  `capital` INT NOT NULL,
  `customer_id` INT(11) NOT NULL,
  `service_id` INT NOT NULL,
  `city_id` INT NOT NULL,
  `department_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_company_customer_idx` (`customer_id` ASC),
  INDEX `fk_company_service1_idx` (`service_id` ASC),
  INDEX `fk_company_city1_idx` (`city_id` ASC),
  INDEX `fk_company_department1_idx` (`department_id` ASC),
  CONSTRAINT `fk_company_customer`
    FOREIGN KEY (`customer_id`)
    REFERENCES `bd_test`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_service1`
    FOREIGN KEY (`service_id`)
    REFERENCES `bd_test`.`service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `bd_test`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `bd_test`.`department` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_test`.`datacredito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_test`.`datacredito` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL,
  `evidence` BLOB NOT NULL,
  `customer_id` INT(11) NOT NULL,
  `service_id` INT NOT NULL,
  `city_id` INT NOT NULL,
  `department_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_datacredito_customer1_idx` (`customer_id` ASC),
  INDEX `fk_datacredito_service1_idx` (`service_id` ASC),
  INDEX `fk_datacredito_city1_idx` (`city_id` ASC),
  INDEX `fk_datacredito_department1_idx` (`department_id` ASC),
  CONSTRAINT `fk_datacredito_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `bd_test`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_datacredito_service1`
    FOREIGN KEY (`service_id`)
    REFERENCES `bd_test`.`service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_datacredito_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `bd_test`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_datacredito_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `bd_test`.`department` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `bd_test` ;

-- -----------------------------------------------------
-- Table `bd_test`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_test`.`user` (
  `id` INT(11) NOT NULL,
  `email` VARCHAR(180) NOT NULL,
  `roles` TEXT NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `customer_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UNIQ_8D93D649E7927C74` (`email` ASC),
  INDEX `fk_user_customer_idx` (`customer_id` ASC),
  CONSTRAINT `fk_user_customer`
    FOREIGN KEY (`customer_id`)
    REFERENCES `bd_test`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
